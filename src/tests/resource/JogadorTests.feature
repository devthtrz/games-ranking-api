#language: pt
Funcionalidade: Games Ranking
  
  
  Um grupo de amigos costuma praticar diversas modalidades de jogos
  Em suas conversas o assunto comum é: quem é o melhor e quem ganhou mais partidas
  Foi aí que um deles teve a ideia de preparar um sistema que gravasse um ranking dos jogos
  Esse ranking mostraria o jogador, as vitórias e as partidas que esse jogador participou

  Cenario: Adicionar Jogador
    Quando eu adiciono o seguinte jogador: "José", 30 e 30
    Então vejo a seguinte mensagem: "Jogador gravado com suscesso!"

  Cenario: Adicionar vitória
    Quando eu seleciono os dados do jogador: "José", 30 e 30 para adicionar vitorias
    E adiciono vitória
    Então vejo o jogador: "Jóse", 31, 31

  Cenario: Adicionar partida
    Quando eu seliciono os dados do jogador: "Carlos", 20 e 25 para adicionar partida
    E adiciono partida
    Então vejo o jogador: "Carlos", 21, 26

  Cenario: Visualizar Ranking ordenado pelo número de vitórias
    Quando eu acesso a lista de jogadores cadastrados:
      | jogador | vitorias | partidas |
      | Carlos  |       20 |       25 |
      | José    |       30 |       30 |
      | Camila  |       15 |       35 |
    Então vejo a lista de jogadores ordenada por vitórias:
      | jogador | vitorias | partidas |
      | José    |       30 |       30 |
      | Carlos  |       20 |       25 |
      | Camila  |       15 |       35 |
