package br.com.gamesranking.api.camadas.servico;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.gamesranking.api.model.Jogador;
import br.com.gamesranking.api.repository.JogadorRepository;
import br.com.gamesranking.api.servico.JogadorService;
import br.com.gamesranking.api.servico.exception.DuplicadoJogadorException;
import br.com.gamesranking.api.servico.impl.JogadorServiceImpl;

@RunWith(SpringRunner.class)
public class JogadorServiceTest {

	private static final String JOGADOR = "José";
	private static final int VITORIAS = 30;
	private static final int PARTIDAS = 30;

	@MockBean
	private JogadorRepository jogadorRepository;

	private JogadorService jogadorService;

	private Jogador jogador;

	@Before
	public void setUp() throws Exception {

		jogadorService = new JogadorServiceImpl(jogadorRepository);

		jogador = new Jogador(this.JOGADOR.toUpperCase(), this.VITORIAS, this.PARTIDAS);

		when(jogadorRepository.findByJogador(JOGADOR)).thenReturn(Optional.empty());

	}

	@Test
	public void deve_salvar_o_jogador_no_repositorio() throws Exception {
		
		jogadorService.salvar(jogador);
		
		verify(jogadorRepository).save(jogador);
	}
	
	@Test(expected = DuplicadoJogadorException.class)
	public void nao_deve_salvar_o_jogador_com_o_mesmo_nome() throws Exception {
		
		when(jogadorRepository.findByJogador(JOGADOR.toUpperCase())).thenReturn(Optional.of(jogador));
		
		jogadorService.salvar(jogador);
	}
	
	@Test
	public void deve_adicionar_vitorias() throws Exception {
		
		when(jogadorRepository.findByJogador(JOGADOR.toUpperCase())).thenReturn(Optional.of(jogador));
		
		jogadorService.adicionarVitorias(jogador);
		
	}
	
	@Test
	public void deve_adiconar_partidas() throws Exception {
		
		when(jogadorRepository.findByJogador(JOGADOR.toUpperCase())).thenReturn(Optional.of(jogador));
		
		jogadorService.adicionarPartidas(jogador);
		
	}

}
