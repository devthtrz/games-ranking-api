package br.com.gamesranking.api.camadas.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.gamesranking.api.model.Jogador;
import br.com.gamesranking.api.repository.JogadorRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JogadorRepositoryTest {
	
	
	@Autowired
	private JogadorRepository jogadorRepository;
	
	@Test
	public void deve_encontrar_jogador_pelo_nome() throws Exception {
		Optional<Jogador> optional = jogadorRepository.findByJogador("José");
		assertThat(optional.isPresent()).isTrue();
		
		Jogador jogador = optional.get();
		
		assertThat(jogador.getJogador()).isEqualTo("José");
		
	}

}
