package br.com.gamesranking.api.cucumber;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import br.com.gamesranking.api.model.Jogador;
import cucumber.api.DataTable;
import cucumber.api.java.pt.Então;
import cucumber.api.java.pt.Quando;

public class JogadorSteps {

	private Jogador jogador;
	List<Jogador> jogadores = new ArrayList<Jogador>();
	

	@Quando("^eu adiciono o seguinte jogador: \"([^\"]*)\", (\\d+) e (\\d+)$")
	public void eu_adiciono_o_seguinte_jogador_e(String arg1, int arg2, int arg3) throws Throwable {

		jogador = new Jogador(arg1, arg2, arg3);
	}

	@Então("^vejo a seguinte mensagem: \"([^\"]*)\"$")
	public void vejo_a_seguinte_mensagem(String arg1) throws Throwable {

		assertTrue(arg1, jogador.isSalvo(jogador.getJogador()));

	}

	@Quando("^eu seleciono os dados do jogador: \"([^\"]*)\", (\\d+) e (\\d+) para adicionar vitorias$")
	public void eu_seleciono_os_dados_do_jogador_e_para_adicionar_vitorias(String arg1, int arg2, int arg3)
			throws Throwable {

		jogador = new Jogador(arg1, arg2, arg3);
	}

	@Quando("^adiciono vitória$")
	public void adiciono_vitória() throws Throwable {

		jogador.adicionarVitorias();
	}

	@Então("^vejo o jogador: \"([^\"]*)\", (\\d+), (\\d+)$")
	public void vejo_o_jogador(String arg1, int arg2, int arg3) throws Throwable {

		assertTrue(jogador.getVitorias() == 31 && jogador.getPartidas() == 31
				|| jogador.getVitorias() == 21 && jogador.getPartidas() == 26);

	}

	@Quando("^eu seliciono os dados do jogador: \"([^\"]*)\", (\\d+) e (\\d+) para adicionar partida$")
	public void eu_seliciono_os_dados_do_jogador_e_para_adicionar_partida(String arg1, int arg2, int arg3)
			throws Throwable {
		jogador = new Jogador(arg1, arg2, arg3);
	}

	@Quando("^adiciono partida$")
	public void adiciono_partida() throws Throwable {

		jogador.adicionarPartidas();
	}

	@Quando("^eu acesso a lista de jogadores cadastrados:$")
	public void eu_acesso_a_lista_de_jogadores_cadastrados(DataTable arg1) throws Throwable {

		for (Map<String, String> rows : arg1.asMaps(String.class, String.class)) {
			Jogador jg = new Jogador(rows.get("jogador"), Integer.parseInt(rows.get("vitorias")),
					Integer.parseInt(rows.get("partidas")));
			jogadores.add(jg);
		}

		Collections.sort(jogadores);
	}

	@Então("^vejo a lista de jogadores ordenada por vitórias:$")
	public void vejo_a_lista_de_jogadores_ordenada_por_vitórias(DataTable arg1) throws Throwable {

		List<Jogador> jogadoresOrdenados = new ArrayList<Jogador>();
		
		for (Map<String, String> rows : arg1.asMaps(String.class, String.class)) {
			Jogador jg = new Jogador(rows.get("jogador"), Integer.parseInt(rows.get("vitorias")),
					Integer.parseInt(rows.get("partidas")));
			jogadoresOrdenados.add(jg);
		}
		
		assertTrue(jogadoresOrdenados.get(0).getJogador() == jogadores.get(0).getJogador() && jogadoresOrdenados.get(0).getVitorias() == jogadores.get(0).getVitorias()
				&& jogadoresOrdenados.get(1).getJogador() == jogadores.get(1).getJogador() && jogadoresOrdenados.get(1).getVitorias() == jogadores.get(1).getVitorias()
				&& jogadoresOrdenados.get(2).getJogador() == jogadores.get(2).getJogador() && jogadoresOrdenados.get(2).getVitorias() == jogadores.get(2).getVitorias());

	}

}
