package br.com.gamesranking.api.resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.gamesranking.api.model.Jogador;
import br.com.gamesranking.api.repository.JogadorRepository;
import br.com.gamesranking.api.servico.JogadorService;
import br.com.gamesranking.api.servico.exception.DuplicadoJogadorException;
import br.com.gamesranking.api.servico.exception.JogadorNaoLocalizadoException;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/ranking")
public class JogadorResource {

	@Autowired
	private JogadorService jogadorService;

	@Autowired
	private JogadorRepository jogadorRepository;

	/*
	 * Adicionar jogador: Permitir informar nome, quantidadeVitorias e
	 * quantidadePartidas;
	 */
	@PostMapping
	public ResponseEntity<Jogador> adicionarjogador(@Valid @RequestBody Jogador jogador)
			throws DuplicadoJogadorException {

		final Jogador jogadorSalvo = jogadorService.salvar(jogador);

		return new ResponseEntity<>(jogadorSalvo, HttpStatus.CREATED);

	}

	/*
	 * Adicionar partida: incrementar quantidadeVitorias e quantidadePartidas do
	 * jogador;
	 */
	@PutMapping(value = "/adicionarvitoria")
	public ResponseEntity<?> adicionarVitoria(@Valid @RequestBody Jogador jogador)
			throws JogadorNaoLocalizadoException {

		jogadorService.adicionarVitorias(jogador);
		return ResponseEntity.ok().body("Vitória adicionada com Sucesso!");
	}

	/*
	 * Adicionar partida: incrementar quantidadeVitorias e quantidadePartidas do
	 * jogador;
	 */
	@PutMapping(value = "/adicionarpartida")
	public ResponseEntity<?> adicionarPartida(@Valid @RequestBody Jogador jogador)
			throws JogadorNaoLocalizadoException {

		jogadorService.adicionarPartidas(jogador);
		return ResponseEntity.ok().body("Partida adicionado com Sucesso!");
	}

	/*
	 * Visualizar Ranking (Listar jogadores ordenado pelo número de vitórias)
	 */
	@GetMapping(value = "/lista")
	public ResponseEntity<List<Jogador>> listar() {

		final List<Jogador> jogadores = jogadorRepository.findAll();
		Collections.sort(jogadores);

		if (jogadores.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<List<Jogador>>(new ArrayList<Jogador>(jogadores), HttpStatus.OK);
	}

	@ExceptionHandler({ DuplicadoJogadorException.class })
	public ResponseEntity<Erro> handleDuplicadoJogadorException(DuplicadoJogadorException e) {
		return new ResponseEntity<>(new Erro(e.getMessage()), HttpStatus.NOT_FOUND);
	}

	class Erro {
		private final String erro;

		public Erro(String erro) {
			this.erro = erro;
		}

		public String getErro() {
			return erro;
		}
	}
}
