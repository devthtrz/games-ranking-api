package br.com.gamesranking.api.servico.impl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import br.com.gamesranking.api.model.Jogador;
import br.com.gamesranking.api.repository.JogadorRepository;
import br.com.gamesranking.api.servico.JogadorService;
import br.com.gamesranking.api.servico.exception.DuplicadoJogadorException;
import br.com.gamesranking.api.servico.exception.JogadorNaoLocalizadoException;

@Service
public class JogadorServiceImpl implements JogadorService {

	private final JogadorRepository jogadorRepository;
	
	public JogadorServiceImpl(JogadorRepository jogadorRepository) {
		this.jogadorRepository = jogadorRepository;
	}
	
	@Override
	public Jogador salvar(Jogador jogador) throws DuplicadoJogadorException {
		
		Optional<Jogador> optional = jogadorRepository.findByJogador(jogador.getJogador().toUpperCase());
		
		if (optional.isPresent()) {
			throw new DuplicadoJogadorException("Jogador já existe cadastrado com o nome '" + jogador.getJogador()+ "'");
		}
		jogador.setJogador(jogador.getJogador().toUpperCase());
		return jogadorRepository.save(jogador);
	}
	
	@Override
	public Jogador adicionarVitorias(Jogador jogador) throws JogadorNaoLocalizadoException {

		Optional<Jogador> optional = jogadorRepository.findByJogador(jogador.getJogador().toUpperCase());
		
		if (!optional.isPresent()) {
			
			throw new JogadorNaoLocalizadoException("Jogador não localizado com o nome '"+ jogador.getJogador() + "'");
			
		}
		
		optional.get().adicionarVitorias();
		BeanUtils.copyProperties(jogador,optional, "id");
		
		return jogadorRepository.save(optional.get());
		
		
	}
	
	@Override
	public Jogador adicionarPartidas(Jogador jogador) throws JogadorNaoLocalizadoException {

		Optional<Jogador> optional = jogadorRepository.findByJogador(jogador.getJogador().toUpperCase());
		
		if (!optional.isPresent()) {
			
			throw new JogadorNaoLocalizadoException("Jogador não localizado com o nome '"+ jogador.getJogador() + "'");
			
		}
		
		optional.get().adicionarPartidas();
		
		BeanUtils.copyProperties(jogador,optional, "id");
		return jogadorRepository.save(optional.get());
		
		
	}

}
