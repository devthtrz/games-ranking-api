package br.com.gamesranking.api.servico.exception;

public class DuplicadoJogadorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DuplicadoJogadorException(String message) {
		super(message);
	}
}
