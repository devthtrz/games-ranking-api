package br.com.gamesranking.api.servico.exception;

public class JogadorNaoLocalizadoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JogadorNaoLocalizadoException(String message) {
		super(message);
	}

}
