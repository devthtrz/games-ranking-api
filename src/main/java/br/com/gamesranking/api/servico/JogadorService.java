package br.com.gamesranking.api.servico;

import br.com.gamesranking.api.model.Jogador;
import br.com.gamesranking.api.servico.exception.DuplicadoJogadorException;
import br.com.gamesranking.api.servico.exception.JogadorNaoLocalizadoException;

public interface JogadorService {
	
	Jogador salvar(Jogador jogador) throws DuplicadoJogadorException;

	Jogador adicionarPartidas(Jogador jogador) throws JogadorNaoLocalizadoException;
	
	Jogador adicionarVitorias(Jogador jogador) throws JogadorNaoLocalizadoException;

}
